#!/usr/bin/env bash

set -e

################### constants ###################

RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
BLUE='\033[0;34m'
PURPLE='\033[0;35m'
CYAN='\033[0;36m'

DRED='\033[1;31m'
DGREEN='\033[1;32m'
DYELLOW='\033[1;33m'
DBLUE='\033[1;34m'
DPURPLE='\033[1;35m'
DCYAN='\033[1;36m'

NC='\033[0m'

################### parse parameters ###################

DEFAULT_ENV="dev"
DEFAULT_PORT=8080
DEFAULT_GIN_MODE="debug"
DEFAULT_GQL_COMPLEXITY_LIMIT="20"
DEFAULT_LRU_CACHE_CAPACITY="10"
DEFAULT_LRU_CACHE_EXPIRY="5"

set_default=false
log_level=info

for kv in "$@"; do
  kv=(${kv/=/ })
  case ${kv[0]} in
    --set_default)
      set_default=true
      ;;
    --log_level)
      log_level="${kv[@]:1}"
      ;;
      *)
      echo -e "unkown param: ${YELLOW}${kv[@]}${NC}, skipping"
  esac
done

################### funcs/vars ###################

infoMsg() {
  if [ "$(echo ${log_level} | tr A-Z a-z)" == "info" ] && [ "$(echo ${set_default} | tr A-Z a-z)" != "true" ]; then
    echo -e "$1"
  fi
}

errorMsg() {
  if [ "$(echo ${log_level} | tr A-Z a-z)" != "none" ]; then
    echo -e "$1"
  fi
}

get_default() {
  local val=$(eval "echo \$DEFAULT_$(echo ${1} | tr a-z A-Z)")
  echo $val
}

check_dep() {
  local ENVS=("OWM_API_KEY")
  local OPTIONAL_ENV=("ENV" "GIN_MODE" "PORT" "GQL_COMPLEXITY_LIMIT" "LRU_CACHE_CAPACITY" "LRU_CACHE_EXPIRY")
  local missing=()
  local found=()
  local optional=()

  for g in "${ENVS[@]}"; do
    g=($g)
    local found_g=()
    for e in ${g[@]}; do
      if [ ! "$(echo ${!e})" ]; then
        continue
      fi
      found_g+=($e)
    done
    if [ ${#found_g[@]} == 0 ]; then
      g=${g[@]}
      missing+=("${g// /|}")
      continue
    fi
    found_g=${found_g[@]}
    found+=("${found_g// /|}")
  done

  for g in "${OPTIONAL_ENV[@]}"; do
    g=($g)
    local found_g=()
    for e in ${g[@]}; do
      if [ ! "$(echo ${!e})" ]; then
        continue
      fi
      found_g+=($e)
    done
    if [ ${#found_g[@]} == 0 ]; then
      g=${g[@]}
      optional+=("${g// /|}")
      continue
    fi
    found_g=${found_g[@]}
    found+=("${found_g// /|}")
  done

  if [ ${#found[@]} -gt 0 ]; then
    infoMsg "${DGREEN}found${NC}:"
    for g in "${found[@]}"; do
      local g=(${g//|/ })
      if [ ${#g[@]} -gt 1 ]; then
        local d=${g[@]}
        infoMsg "  ${DCYAN}* ${d// /|}${NC}:"
        for e in ${g[@]}; do
          infoMsg "    ${CYAN}- ${e}=${!e}${NC}"
        done
        continue
      fi
      for e in ${g[@]}; do
        infoMsg "  ${CYAN}* ${e}=${!e}${NC}"
      done
    done
  fi

  if [ ${#optional[@]} -gt 0 ]; then
    infoMsg "${DYELLOW}optional${NC}:"
    for g in "${optional[@]}"; do
      local g=(${g//|/ })
      if [ ${#g[@]} -gt 1 ]; then
        local d=${g[@]}
        infoMsg "  ${DPURPLE}* ${d// /|}${NC}:"
        local kv=""
        for e in ${g[@]}; do
          kv="${kv}
${e} $(get_default ${e})"
        done
        kv=$(echo "${kv}" | sed '/^\s*$/d' | sort -k2 -k1)
        local v_uniq=$(echo "${kv}" | awk '{print $2}' | uniq -c | awk '{print  $1 " " $2}')
        local k=($(echo "${kv}" | awk '{print $1}'))
        local count=0
        for c in $(echo "${v_uniq[@]}" | awk '{print $1}'); do
          local a=${k[@]:$count:$c}
          a=${a// /,}
          infoMsg "    ${PURPLE}- ${a} (default: $(get_default ${k[$count]}))${NC}"
          for _a in ${a//,/ }; do
            if [ "$(echo ${set_default} | tr A-Z a-z)" == "true" ]; then
              echo "export ${_a}=\"$(get_default ${_a})\""
            fi
          done
          count=$((count + c))
        done
        continue
      fi
      for e in ${g[@]}; do
        infoMsg "  ${PURPLE}* ${e} (default: $(get_default ${e}))${NC}"
        if [ "$(echo ${set_default} | tr A-Z a-z)" == "true" ]; then
          echo "export ${e}=\"$(get_default ${e})\""
        fi
      done
    done
  fi

  if [ ${#missing[@]} -gt 0 ]; then
    errorMsg "${DRED}missing${NC}:"
    for e in ${missing[@]}; do
      errorMsg "  ${YELLOW}* ${e}${NC}"
    done
    exit 1
  fi
}

################### core ###################

check_dep
