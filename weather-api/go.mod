module weather-api

go 1.13

require (
	github.com/99designs/gqlgen v0.11.3
	github.com/briandowns/openweathermap v0.16.0
	github.com/cloudflare/golibs v0.0.0-20190417125240-4efefffc6d5c
	github.com/gin-contrib/cors v1.3.1 // indirect
	github.com/gin-gonic/gin v1.6.3
	github.com/joho/godotenv v1.3.0
	github.com/vektah/gqlparser/v2 v2.0.1
)
