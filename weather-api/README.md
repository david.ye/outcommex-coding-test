## Overview
weather-api

## Requirements
* golang (>=1.12)

## ENV
* **`OWM_API_KEY`** (**required**, description: "openweathermap api key (refer to https://openweathermap.org/appid)"
* GIN_MODE (optional, default: "debug")
* PORT (optional, default: "8080")
* GQL_COMPLEXITY_LIMIT (optional, default: "20", description: "max number of gql query complexity")
* LRU_CACHE_CAPACITY (optional, default: "10", description: "max capacity of openweatherapi city response to be cahed (least accessed city response will be removed, if overflows)")
* LRU_CACHE_EXPIRY (optional, default: "5", description: "the interval openweatherapi city response should be cached (in minutes)")

## Usage
```bash
# run server
go run server.go
```

```bash
# build binary
go build -o weather-api
```

## Highlights
* graphql rest framework made with gin and gqlgen
* dotenv used to support `.env`
* cloudflare `lrucache` library used to cache frequent requested city request for `LRU_CACHE_EXPIRY` (default: 5) miniutes, with max capacity of `LRU_CACHE_CAPACITY` (default: 20), when lrucache capacity overflows least frequent accessed cache (indexed by normalized city name) will be removed
* in production mode `GIN_MODE=release`, `GQL_COMPLEXITY_LIMIT` (defaults: 20) environment variable can be set to prevent gql request overflow ddos attack.

## References
* https://hellokoding.com/golang-convert-string-to-int/
* https://github.com/cloudflare/golibs/blob/master/lrucache/lrucache.go
* https://stackoverflow.com/questions/37274282/regex-with-replace-in-golang
* https://gqlgen.com/getting-started/
* https://gqlgen.com/recipes/gin/
* https://gqlgen.com/reference/errors/
* https://github.com/briandowns/openweathermap/blob/master/current.go
* https://github.com/briandowns/openweathermap/blob/master/openweathermap.go
* https://github.com/joho/godotenv
* https://github.com/gin-gonic/gin