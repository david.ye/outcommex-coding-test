#!/usr/bin/env bash

set -e

################### constants ###################

RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
BLUE='\033[0;34m'
PURPLE='\033[0;35m'
CYAN='\033[0;36m'

DRED='\033[1;31m'
DGREEN='\033[1;32m'
DYELLOW='\033[1;33m'
DBLUE='\033[1;34m'
DPURPLE='\033[1;35m'
DCYAN='\033[1;36m'

NC='\033[0m'

################### core ###################

dir_name=$(cd $(dirname $0) && pwd)

# check dep
./dep.sh
eval "$(./dep.sh --set_default)"

# exec cmd
exec "$@"
