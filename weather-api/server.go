package main

import (
	"context"
	"log"
	"os"
	"strconv"

	"weather-api/graph"
	"weather-api/graph/generated"

	owm "github.com/briandowns/openweathermap"
	"github.com/cloudflare/golibs/lrucache"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"

	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/handler/extension"
	"github.com/99designs/gqlgen/graphql/playground"
)

// Defining the Graphql handler
func graphqlHandler() gin.HandlerFunc {
	// NewExecutableSchema and Config are in the generated.go file
	// Resolver is in the resolver.go file
	h := handler.NewDefaultServer(
		generated.NewExecutableSchema(generated.Config{Resolvers: &graph.Resolver{}}),
	)

	return func(c *gin.Context) {
		if gin.Mode() == "release" {
			complexity_limit, _ := strconv.Atoi(graph.EnvWithDefaults("GQL_COMPLEXITY_LIMIT"))
			h.Use(extension.FixedComplexityLimit(complexity_limit))
		}
		h.ServeHTTP(c.Writer, c.Request)
	}
}

// Defining the Playground handler
func playgroundHandler() gin.HandlerFunc {
	h := playground.Handler("GraphQL", "/query")

	return func(c *gin.Context) {
		h.ServeHTTP(c.Writer, c.Request)
	}
}

func GinContextToContextMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx := context.WithValue(c.Request.Context(), "GinContextKey", c)
		c.Request = c.Request.WithContext(ctx)
		c.Next()
	}
}

func OWMCurrentMiddleware(w *owm.CurrentWeatherData) gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Set("OWMCurrentKey", w)
		c.Next()
	}
}

func LRUCacheMiddleware(cache *lrucache.LRUCache) gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Set("LRUCacheKey", cache)
		c.Next()
	}
}

func main() {
	// Setting up Gin
	godotenv.Load()

	gin.SetMode(os.Getenv("GIN_MODE"))

	owm, err := owm.NewCurrent("C", "en", os.Getenv("OWM_API_KEY"))
	if err != nil {
		log.Fatal("Error loading own.CurrentWeatherData")
	}

	r := gin.Default()
	r.Use(GinContextToContextMiddleware())
	r.Use(OWMCurrentMiddleware(owm))
	capacity, _ := strconv.Atoi(graph.EnvWithDefaults("LRU_CACHE_CAPACITY"))
	r.Use(LRUCacheMiddleware(lrucache.NewLRUCache(uint(capacity))))
	// config := cors.DefaultConfig()
	// config.AllowOrigins = []string{"http://localhost:3000", "http://localhost:8080"}
	r.Use(cors.Default())
	r.POST("/query", graphqlHandler())
	r.GET("/", playgroundHandler())
	r.Run()
}

// const defaultPort = "8080"

// func main() {
// 	port := os.Getenv("PORT")
// 	if port == "" {
// 		port = defaultPort
// 	}

// 	srv := handler.NewDefaultServer(generated.NewExecutableSchema(generated.Config{Resolvers: &graph.Resolver{}}))

// 	http.Handle("/", playground.Handler("GraphQL playground", "/query"))
// 	http.Handle("/query", srv)

// 	log.Printf("connect to http://localhost:%s/ for GraphQL playground", port)
// 	log.Fatal(http.ListenAndServe(":"+port, nil))
// }
