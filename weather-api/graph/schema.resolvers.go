package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"strconv"
	"time"
	"weather-api/graph/generated"
	"weather-api/graph/model"

	"github.com/99designs/gqlgen/graphql"
	"github.com/vektah/gqlparser/v2/gqlerror"
)

func (r *mutationResolver) CreateTodo(ctx context.Context, input model.NewTodo) (*model.Todo, error) {
	// gc, err := GinContextFromContext(ctx)
	// if err != nil {
	// 	return nil, err
	// }

	todo := &model.Todo{
		Text: input.Text,
		ID:   fmt.Sprintf("T%d", rand.Int()),
		User: &model.User{ID: input.UserID, Name: "user " + input.UserID},
	}
	r.todos = append(r.todos, todo)
	return todo, nil
}

func (r *queryResolver) Todos(ctx context.Context) ([]*model.Todo, error) {
	return r.todos, nil
}

func (r *queryResolver) Weathers(ctx context.Context, locations []*string) ([]*model.CurrentForecast, error) {
	gc, err := GinContextFromContext(ctx)
	if err != nil {
		return nil, err
	}

	w, err := OWMCurrentFromContext(gc)
	if err != nil {
		return nil, err
	}

	cache, err := LRUCacheFromContext(gc)
	if err != nil {
		return nil, err
	}

	current_forcasts := make([]*model.CurrentForecast, 0, len(locations))

	for i, location := range locations {
		_location := StandardizeString(*location)
		log.Println(fmt.Sprintf("obtaining CurrentForecast on `%s` ...", _location))

		_current_forcast, exists, expired := cache.GetStale(_location)

		if exists && !expired {
			log.Println(fmt.Sprintf("using `%s` CurrentForecast data found in cache", _location))
			current_forcast := _current_forcast.(*model.CurrentForecast)
			current_forcast.Idx = i
			current_forcasts = append(current_forcasts, current_forcast)
			log.Println(fmt.Sprintf("obtaining CurrentForecast on `%s` ... done", _location))
			continue
		}
		log.Println(fmt.Sprintf("getting `%s` CurrentForecast from openweatherapi ...", _location))
		err := w.CurrentByName(_location)

		if err != nil {
			graphql.AddError(ctx, &gqlerror.Error{
				Message: fmt.Sprintf("Invalid address: %s", _location),
				Extensions: map[string]interface{}{
					"idx":      i,
					"location": *location,
					"err":      err,
				},
			})
			err_str, _ := json.Marshal(err)
			log.Println(fmt.Sprintf("error caling `owm.CurrentByName` on `%s`: %s", _location, err_str))
			log.Println(fmt.Sprintf("getting `%s` CurrentForecast from openweatherapi ... fail", _location))
			continue
		}

		log.Println(fmt.Sprintf("getting `%s` CurrentForecast from openweatherapi ... done", _location))
		wB, _ := json.Marshal(w)
		snowB, _ := json.Marshal(w.Snow)
		mainB, _ := json.Marshal(w.Main)
		rainB, _ := json.Marshal(w.Rain)
		sysB, _ := json.Marshal(w.Sys)
		weathers := make([]*model.Weather, len(w.Weather))

		for _i, weather := range w.Weather {
			weathers[_i] = &model.Weather{
				ID:          fmt.Sprintf("%d", weather.ID),
				Icon:        weather.Icon,
				Main:        weather.Main,
				Description: weather.Description,
			}
		}

		current_forcast := &model.CurrentForecast{
			ID:       fmt.Sprintf("%d", w.ID),
			Text:     string(wB),
			Rain:     string(rainB),
			Main:     string(mainB),
			Snow:     string(snowB),
			Sys:      string(sysB),
			Timezone: w.Timezone,
			Weather:  weathers,
		}

		expiry, _ := strconv.Atoi(EnvWithDefaults("LRU_CACHE_EXPIRY"))
		expiry_time := time.Now().Add(time.Minute * time.Duration(expiry))
		log.Println(fmt.Sprintf("saving `%s` CurrentForecast data to cache, expire at `%s`", _location, expiry_time))
		cache.Set(_location, current_forcast, expiry_time)
		log.Println(fmt.Sprintf("saving `%s` CurrentForecast data to cache, expire at `%s` ... done", _location, expiry_time))

		current_forcast.Idx = i
		current_forcasts = append(current_forcasts, current_forcast)
		log.Println(fmt.Sprintf("obtaining CurrentForecast on `%s` ... done", _location))
	}

	return current_forcasts, nil
}

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }
