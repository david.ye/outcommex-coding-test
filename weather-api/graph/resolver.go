package graph

//go:generate go run github.com/99designs/gqlgen

import (
	"context"
	"fmt"
	"os"
	"regexp"
	"strings"
	"weather-api/graph/model"

	owm "github.com/briandowns/openweathermap"
	"github.com/cloudflare/golibs/lrucache"
	"github.com/gin-gonic/gin"
)

// This file will not be regenerated automatically.
//
// It serves as dependency injection for your app, add any dependencies you require here.

type Resolver struct {
	todos []*model.Todo
}

func GinContextFromContext(ctx context.Context) (*gin.Context, error) {
	ginContext := ctx.Value("GinContextKey")
	if ginContext == nil {
		err := fmt.Errorf("could not retrieve gin.Context")
		return nil, err
	}

	gc, ok := ginContext.(*gin.Context)
	if !ok {
		err := fmt.Errorf("gin.Context has wrong type")
		return nil, err
	}
	return gc, nil
}

func OWMCurrentFromContext(ctx *gin.Context) (*owm.CurrentWeatherData, error) {
	_w := ctx.MustGet("OWMCurrentKey")
	if _w == nil {
		err := fmt.Errorf("could not retrieve owm.CurrentWeatherData")
		return nil, err
	}

	wf, ok := _w.(*owm.CurrentWeatherData)
	if !ok {
		err := fmt.Errorf("owm.CurrentWeatherData has wrong type")
		return nil, err
	}
	return wf, nil
}

func LRUCacheFromContext(ctx *gin.Context) (*lrucache.LRUCache, error) {
	_cache := ctx.MustGet("LRUCacheKey")
	if _cache == nil {
		err := fmt.Errorf("could not retrieve lrucache.LRUCache")
		return nil, err
	}

	cache, ok := _cache.(*lrucache.LRUCache)
	if !ok {
		err := fmt.Errorf("lrucache.LRUCache has wrong type")
		return nil, err
	}
	return cache, nil
}

func StandardizeString(s string) string {
	_s := strings.ToLower(s)
	_s = strings.Trim(_s, " ")
	re := regexp.MustCompile(` +`)
	_s = re.ReplaceAllString(_s, " ")
	re = regexp.MustCompile(`,([^ ])`)
	_s = re.ReplaceAllString(_s, ", $1")

	return _s
}

func EnvWithDefaults(s string) string {
	defaults := map[string]string{
		"GQL_COMPLEXITY_LIMIT": "20",
		"LRU_CACHE_CAPACITY":   "10",
		"LRU_CACHE_EXPIRY":     "5",
	}

	v, exists := os.LookupEnv(s)
	if !exists {
		_v, _ := defaults[s]
		return _v
	}

	return v
}
