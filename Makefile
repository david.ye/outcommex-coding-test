#!/usr/bin/env make

ifneq ("$(wildcard .env)","")
  VARS :=$(shell sed '/^[[:space:]]*$$/d' .env | sed -ne 's/ *\#.*$$//; /./ s/=.*$$// p' | grep -v "$$(env | cut -f1 -d'=' | sed -E 's/^(.+)$$/^\1$$/g')")
  $(foreach v,$(VARS),$(eval $(shell echo export $(v)="$(shell sed '/^[[:space:]]*$$/d' .env | grep "^$(v)=" | cut -f2 -d'=')")))
endif

#@ Envs
OPTIONS ?=

build:
	./action.sh --action=build $(OPTIONS)

up:
	./action.sh --action=up $(OPTIONS)

down:
	./action.sh --action=down $(OPTIONS)