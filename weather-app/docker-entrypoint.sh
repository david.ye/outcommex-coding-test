#!/usr/bin/env sh

set -e

################### constants ###################

RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
BLUE='\033[0;34m'
PURPLE='\033[0;35m'
CYAN='\033[0;36m'

DRED='\033[1;31m'
DGREEN='\033[1;32m'
DYELLOW='\033[1;33m'
DBLUE='\033[1;34m'
DPURPLE='\033[1;35m'
DCYAN='\033[1;36m'

NC='\033[0m'

NGINX_ETC=/etc/nginx
HTML_DIR=/usr/share/nginx/html

################### core ###################

PORT=${PORT:-3000}
GRAPHQL_ENDPOINT=${GRAPHQL_ENDPOINT:-http://weather-api:8000/query}

echo -e "${DBLUE}params${NC}:"
echo -e "  ${CYAN}* ENV=${ENV}${NC}"
echo -e "  ${CYAN}* PORT=${PORT}${NC}"
echo -e "  ${CYAN}* GRAPHQL_ENDPOINT=${GRAPHQL_ENDPOINT}${NC}"
echo -e "  ${CYAN}* GOOGLE_API_KEY=${GOOGLE_API_KEY}${NC}"

GRAPHQL_ENDPOINT=${GRAPHQL_ENDPOINT//\//\\/}
GOOGLE_API_KEY=${GOOGLE_API_KEY//\//\\/}

if [ "$(echo ${ENV})" != "dev" ]; then
  rm static/**/*.js.map
  rm static/**/*.css.map
fi

sed -i -E "s/^(user\s+.+)$/#\1/g" ${NGINX_ETC}/nginx.conf
sed -i -E "s/__GRAPHQL_ENDPOINT__/${GRAPHQL_ENDPOINT}/g" ${HTML_DIR}/static/js/*.js
sed -i -E "s/__GOOGLE_API_KEY__/${GOOGLE_API_KEY}/g" ${HTML_DIR}/static/js/*.js
sed -i -E "s/^(\s*)listen\s+\d+\s*;\s*$/\1listen ${PORT};/g" ${NGINX_ETC}/conf.d/default.conf
sed -i -E "s/^(\s*)index(\s+).+;\s*$/\1try_files \$uri \/index.html;/g" ${NGINX_ETC}/conf.d/default.conf
exec "$@"