# Overview

`weather-app` project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Requirements
* node (>=v1.12)
* yarn (>=1.22)

## ENV
* **`REACT_APP_GOOGLE_API_KEY`** (**required**, description: "google api key (map) (refer to https://developers.google.com/maps/documentation/javascript/get-api-key)")
* `REACT_APP_GRAPHQL_ENDPOINT` (optional, default: "http://localhost:8080/query", description: "backend graphql endpoint")

## Usage
In the project directory, you can run:

### `yarn start`

Runs the app in the development mode. Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `yarn test`

Launches the test runner in the interactive watch mode. See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder. It correctly bundles React in production mode and optimizes the build for the best performance.

## Highlights
* create-react-app is used to template the react framework
* apollo-graphql react library is used to integrate react with `REACT_APP_GRAPHQL_ENDPOINT` graphql backend
* google map react library `GoogleApiWrapper` is used as a wrapper for access too google map api, this then gets used during react life cycle to autocomplete current input in the city search dropdown. (`REACT_APP_GOOGLE_API_KEY` is **required** for this to work)
* semantic ui react library used to achieve multi input dropdown and also the core css of the app

## References
* https://github.com/kentcdodds/cross-env
* https://awesomereact.com/playlists/slack-clone-using-graphql/yGaIfZSakkI
* https://www.apollographql.com/docs/react/get-started/
* https://www.apollographql.com/docs/react/api/react-hoc/
* https://developers.google.com/maps/documentation/javascript/reference/places-autocomplete-service
* https://developers.google.com/maps/documentation/javascript/examples/places-queryprediction
* https://github.com/apollographql/react-apollo/blob/master/examples/base/src/index.js
* https://www.prisma.io/blog/how-to-use-create-react-app-with-graphql-apollo-62e574617cff
