import React from 'react';
// import './App.css';
// import compose from 'lodash/flowRight';
import 'semantic-ui-css/semantic.min.css';
// import { graphql } from '@apollo/react-hoc';
import { WEATHERS } from './schemas/weather';
import debounce from 'lodash/debounce';
import {
  Container,
  Label,
  Button,
  Dropdown,
  Card,
  Image,
  Divider,
  Header,
  Icon,
  Transition,
  Loader,
  Message,
  Segment,
  Flag,
} from 'semantic-ui-react';
import { GoogleApiWrapper } from 'google-maps-react';
import voca from 'voca';

const placeCorrection = (s) => {
  if (typeof s !== 'string') {
    return ""
  }

  return s.replace(/taiwan/gi, 'China, Taiwan').replace(/(hong kong)|hongkong/gi, 'China, Hong Kong').replace(/macao/gi, 'China, Macau')
}

class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: false,
      locations: [],
      addresses: [],
      currentAddresses: [],
      weathers: {},
      autocompleteService: new this.props.google.maps.places.AutocompleteService(),
    }
  }

  handleAddition = (e, { value }) => {
    if (this.state.addresses.filter((x) => x.text === value.trim() && x.value === value.trim()).length === 0) {
      this.setState({
        addresses: [{ text: value.trim(), value: value.trim() }, ...this.state.addresses]
      });
    }
  };

  handleChange = (e, { value }) => {
    this.setState({
      currentAddresses: value.map((v) => v.trim()).reduce((v, _v) => {
        if (!v.includes(_v)) {
          return [...v, _v];
        } else {
          return v;
        }
      }, [])
    });
  }

  lookupAddress = debounce(async (v) => {
    if (v.trim().length === 0) {
      this.setState({
        loading: false,
        addresses: this.state.currentAddresses.map(v => ({ key: v, text: v, value: v }))
      });
      return;
    }

    this.state.autocompleteService.getPlacePredictions({input: v}, (predictions, status) => {
      this.setState({
        loading: false,
        addresses: Array.isArray(predictions) ? [
          ...predictions.map(p => {
            const address = `${placeCorrection(p.structured_formatting.main_text)}, ${placeCorrection(p.structured_formatting.secondary_text)}`.trim().replace(/,+$/, "")
            return { key: p.id, text: address, value: address, flag: this.getFlag(placeCorrection(p.terms[p.terms.length - 1].value).split(",").reverse().join(",")) }
          }),
          ...this.state.currentAddresses.map(v => ({ key: v, text: v, value: v }))
        ] : this.state.currentAddresses.map(v => ({ key: v, text: v, value: v }))
      })
    })
  }, 500, {leading: false, trailing: true})

  searchWeathers = debounce(async (v) => {
    await this.setState({
      weathers: {...this.state.weathers, ...this.state.currentAddresses.reduce((d, _d) => ({...d, [_d]: {...this.state.weathers[_d], loaded: 'loading'}}), {})}
    })
    const { data, errors } = await this.props.client.query({
      query: WEATHERS,
      variables: {
        locations: this.state.currentAddresses,
      },
      errorPolicy: 'all'
    })

    await this.setState({
      weathers: {...this.state.weathers, ...data.weathers.reduce((d, _d) => ({...d, [this.state.currentAddresses[_d.idx]]: {..._d, loaded: 'success'}}), {})},
    })

    if (errors) {
      await this.setState({
        weathers: {...this.state.weathers, ...Object.keys(errors).reduce((e, _e) => ({...e, [this.state.currentAddresses[errors[_e].extensions.idx]]: {loaded: 'failed', error: errors[_e]}}), {})},
      })
    } else {
      const addrs = this.state.currentAddresses.filter((_, idx) => !data.weathers.map(d => d.idx).includes(idx))
      await this.setState({
        weathers: {...this.state.weathers, ...addrs.reduce((d, _d) => ({...d, [_d]: {...this.state.weathers[_d], loaded: 'failed'}}), {})}
      })
    }
  }, 500)

  searchWeather = debounce((location) => {
    this.props.client.query({
      query: WEATHERS,
      variables: {
        locations: [location],
      },
    }).then(({ data, errors }) => {
      if (! errors) {
        this.setState({
          weathers: {...this.state.weathers, [location]: {...data.weathers[0], loaded: 'success'}},
        })
      }
    }).catch(error => {
      this.setState({
        weathers: {...this.state.weathers, [location]: {loaded: 'failed', error: error}},
      })
    })
  }, 500)

  addressWeatherSuccess = location => typeof this.state.weathers[location] === 'object' && this.state.weathers[location].loaded === 'success'

  addressWeatherFailed = location => typeof this.state.weathers[location] === 'object' && this.state.weathers[location].loaded === 'failed'

  addressWeatherLoading = location => (!this.state.weathers[location]) || (typeof this.state.weathers[location] === 'object' && this.state.weathers[location].loaded !== 'success' && this.state.weathers[location].loaded !== 'failed')

  getColorStatus = (location) => {
    if (this.addressWeatherSuccess(location)) {
      return 'green'
    }

    if (this.addressWeatherFailed(location)) {
      return 'red'
    }

    return 'grey'
  }

  removeAddress = async (index) => {
    await this.setState({
      currentAddresses: this.state.currentAddresses.filter((_, idx) => (idx !== index)),
    })
  }

  // updateAddressStatus = location => {
  //   const idx = addresses
  // }

  formatData(data) {
    return Object.keys(data).reduce((s, d) => {
      return `${s}, ${d}: ${data[d]}`
    }, "").substr(2)
  }

  convertTimeZone(tz) {
    const hour=Math.floor(tz/3600)
    const minute=(tz/3600 - Math.floor(tz/3600)) * 60
    return voca.sprintf('%02d:%02d', hour, minute)
  }

  getFlag(location) {
    const terms = location.split(",")
    let flag = terms[terms.length - 1].trim().toLowerCase()
    if (flag === 'usa') {
      flag = 'us'
    }

    if (flag === 'taiwan' || flag === 'tw') {
      flag = 'china'
    }

    if (flag === 'hong kong' || flag === 'hk') {
      flag = 'china'
    }

    if (flag === 'macau' || flag === 'mo') {
      flag = 'china'
    }

    if (flag === 'hong kong' || flag === 'hk') {
      flag = 'china'
    }

    return flag
  }

  // lookupAddress = (e) => {
  //   this.setState({loading: true})
  //   console.log(e)
  //   debounce(async () => {
  //     console.log("lala")
  //     this.setState({loading: false})
  //   }, 500, {leading: false, trailing: true})()
  // }

  render() {
    return (
      <Container>
        <Button as='div' labelPosition='left' fluid>
          <Dropdown
              placeholder='City'
              fluid
              multiple
              selection
              search
              allowAdditions
              value={this.state.currentAddresses}
              options={this.state.addresses}
              onAddItem={this.handleAddition}
              onChange={this.handleChange}
              onSearchChange={async (e) => {
                this.setState({loading: true})
                await this.lookupAddress(e.target.value)
              }}
              clearable
              loading={this.state.loading}
            />
          <Button icon='refresh' onClick={this.searchWeathers}></Button>
        </Button>

        <Divider horizontal>
          <Transition duration={{hide: 500, show: 500}} visible={this.state.currentAddresses.length > 0}>
            <Header as='h4'>
              <Icon name='cloud' />
                Weathers
            </Header>
          </Transition>
        </Divider>
        <Card.Group itemsPerRow={3}>
          {this.state.currentAddresses.map((address, index) => {
            this.searchWeather(address)
            return (
              <Card
                onClick={debounce(async () => {
                  await this.setState({
                    weathers: {...this.state.weathers, [address]: {...this.state.weathers[address], loaded: 'loading'}}
                  })
                  this.searchWeather(address)
                }, 500, {leading: true, trailing: false})}
                key={index}
                color={this.getColorStatus(address)}
                label={{ as: 'a', corner: 'left', icon: 'heart' }}
              >
                <Container>
                  <div className='right floated'>
                    <Icon name='close' color='grey' onClick={async () => {await this.removeAddress(index)}} />
                  </div>
                </Container>

                <Card.Content>
                  {this.addressWeatherSuccess(address) &&
                    <Image
                      floated='right'
                      size='mini'
                      src={`http://openweathermap.org/img/w/${this.state.weathers[address].weather[0].icon}.png`}
                    />
                  }
                  {this.addressWeatherSuccess(address) &&
                    <div className='right floated'>
                      <Icon
                        name='clock'
                      />
                      {this.convertTimeZone(this.state.weathers[address].timezone)}
                    </div>
                  }
                  <Card.Header><Flag name={this.getFlag(address)} /> {address}</Card.Header>
                  {!this.addressWeatherFailed(address) && (
                    <Segment>
                      <Loader content='Loading' active={this.addressWeatherLoading(address)} inline='centered' />
                      <Card.Meta hidden={!this.addressWeatherSuccess(address)}>
                        <Label.Group>
                          <Label>
                            <Icon name='thermometer half'/>
                            <Label.Detail>{this.addressWeatherSuccess(address) && JSON.parse(this.state.weathers[address].main).temp}</Label.Detail>
                          </Label>
                          <Label>
                            <Icon name='rain'/>
                            <Label.Detail>{this.addressWeatherSuccess(address) && this.formatData(JSON.parse(this.state.weathers[address].rain))}</Label.Detail>
                          </Label>
                          <Label>
                            <Icon name='snowflake'/>
                            <Label.Detail>{this.addressWeatherSuccess(address) && this.formatData(JSON.parse(this.state.weathers[address].snow))}</Label.Detail>
                          </Label>
                        </Label.Group>
                      </Card.Meta>
                      <Card.Description hidden={!this.addressWeatherSuccess(address)}>
                      <b>{this.addressWeatherSuccess(address) && this.state.weathers[address].weather[0].description}</b>
                      </Card.Description>
                    </Segment>
                  )}
                  {this.addressWeatherFailed(address) && (
                    <Segment>
                      <Message icon>
                        <Icon name='exclamation' color='red' />
                        <Message.Content>
                          error fetching data on <b>{address}</b>
                        </Message.Content>
                      </Message>
                    </Segment>

                  )}
                </Card.Content>
                <Card.Content extra>
                  {this.addressWeatherFailed(address) && (this.state.weathers[address].error && this.state.weathers[address].error.message) }
                </Card.Content>
              </Card>
            )
          })}
        </Card.Group>
        {/* <Input
          action={
            { icon: 'search', onClick: (e) => {console.log(this.state.locations)} }
          }
          placeholder='Search...'
          name="locations"
          onChange={
            (e) => {this.setState({[e.target.name]: e.target.value.split("|")})}
          }
          multiple
          fluid
        /> */}
      </Container>
    );
  }
}

export default GoogleApiWrapper({
  apiKey: process.env.REACT_APP_GOOGLE_API_KEY || "__GOOGLE_API_KEY__",
})(App);
