import gql from 'graphql-tag';

export const WEATHERS = gql`
  query weathers($locations: [String!]!) {
    weathers(locations: $locations) {
      weather {
        id
        main
        description
        icon
      }
      snow
      main
      rain
      sys
      timezone
      idx
    }
  }
`