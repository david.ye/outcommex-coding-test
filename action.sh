#!/usr/bin/env bash

set -e

dir_name=$(cd $(dirname $0) && pwd)

################### parse parameters ###################

params=()
SUDO=""

for kv in "$@"; do
  case "${kv}" in
    --action=*)
      action="${kv#*=}"
      ;;
    *)
      if [ "$(echo ${kv})" ]; then
        params+=("${kv}")
      fi
      ;;
  esac
done

################### core ###################

if [ "$(uname -s)" == "Linux" ]; then
  SUDO="sudo -E"
fi

case "${action}" in
  build)
    ${dir_name}/dep.sh --action=${action}
    eval "$(${dir_name}/dep.sh --set_default --action=${build})"

    ${SUDO} docker-compose build "${params[@]}"
    ;;
  up)
    ${dir_name}/dep.sh --action=${action}
    eval "$(${dir_name}/dep.sh --set_default --action=${action})"


    ${SUDO} docker-compose up "${params[@]}"
    ;;
  down)
    ${dir_name}/dep.sh --action=${action}
    eval "$(${dir_name}/dep.sh --set_default --action=${action})"

    ${SUDO} docker-compose stop "${params[@]}"
    ;;
  *)
    echo -e "unkown action: ${YELLOW}${action}${NC}, exiting"
    ;;
esac