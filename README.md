# Overview
weather app made with react, apollo-graphql, gin-gplgen and 🚀

![weather demo](./readme/weather-demo.png)

## Requirements
* **`docker`** (**required**)
* **`make`** (**required**)
* **`docker-compose`** (**required**)
* for `weather-app` development refer to [weather-app/README.md#requirements](./weather-app/README.md#requirements) (optional, only needed for development)
* for `weather-api` development refer to [weather-api/README.md#requirements](./weather-api/README.md#requirements) (optional, only needed for development)

## ENV
* **`GOOGLE_API_KEY`** (**required**, description: "google api key (map) (refer to https://developers.google.com/maps/documentation/javascript/get-api-key)")
* **`OWM_API_KEY`** (**required**, description: "openweathermap api key (refer to https://openweathermap.org/appid")
* APP_PORT (optional, default: "3000", description: "weather-app host binded port")
* API_PORT (optional, default: "8080", description: "weather-api host binded port")
* TAG (optional, default: "dev", description: "docker tag to build/run")

## Usage
```bash
# build docker images
make build
```

```bash
# run local docker stack
make up
# Ctrl+C to stop the docker stack
```

```bash
# run local docker stack in daemon mode
OPTIONS="-d" make up
```

```bash
# stop local docker stack
make down
```

## Highlights
* for `weather-app` refer to [weather-app/README.md#highlights](./weather-app/README.md#highlights)
* for `weather-api` refer to [weather-api/README.md#highlights](./weather-api/README.md#highlights)


## References
* for `weather-app` refer to [weather-app/README.md#references](./weather-app/README.md#references)
* for `weather-api` refer to [weather-api/README.md#references](./weather-api/README.md#references)